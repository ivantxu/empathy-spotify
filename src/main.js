import App from './App.vue';
import Vue from 'vue';
import router from './router';
import store from './store';

import { Spotify } from '@/shared/spotify.js';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  async beforeMount() {
    const token = localStorage.getItem('access_token');
    const spotifyData = store.getters.spotifyData;
    if (!!token || Spotify.tokenNeedsRefresh()) {
      await Spotify.getToken(spotifyData);
    }
  },
  render: (h) => h(App)
}).$mount('#app');
