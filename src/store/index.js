import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const port = window.location.port ? `:${window.location.port}` : '';

const state = () => ({
  spotifyData: {
    clientId: 'ae6a3a31df5a427ab8cbcaec86f42b0b',
    clientSecret: 'e6c1631f430244faba0eaddbfbfb2096',
    redirectUri: `http://${window.location.hostname}${port}/callback`,
    scope: 'user-read-private user-read-email'
  }
});
const mutations = {};
const actions = {};
const getters = {
  spotifyData: (state) => state.spotifyData
};
const modules = {};

export default new Vuex.Store({
  state: state,
  mutations: mutations,
  actions: actions,
  modules: modules,
  getters: getters
});
