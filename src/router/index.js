import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/search',
    name: 'Search',
    component: () => import(/* webpackChunkName: "search" */ '../views/Search.vue')
  },
  {
    path: '/artist',
    name: 'Artist',
    component: () => import(/* webpackChunkName: "artist" */ '../views/Artist.vue')
  },
  {
    path: '/album',
    name: 'Album',
    component: () => import(/* webpackChunkName: "album" */ '../views/Album.vue')
  },
  {
    path: '/track',
    name: 'Track',
    component: () => import(/* webpackChunkName: "track" */ '../views/Track.vue')
  },
  {
    path: '/callback',
    name: 'Callback',
    component: () => import(/* webpackChunkName: "callback" */ '../views/Callback.vue')
  },
  { path: '/', redirect: '/search' }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
