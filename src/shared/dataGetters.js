const artistNameGetter = (item) => item.artists[0].name;
const emptyReturnGetter = () => null;
const genericImageGetter = (item) =>
  item.images && item.images.length > 0
    ? item.images[0].url
    : `${process.env.BASE_URL}no-image.jpg`;
const nameGetter = (item) => item.name;
const trackImageGetter = (track) =>
  track.album.images && track.album.images.length > 0
    ? track.album.images[0].url
    : `${process.env.BASE_URL}no-image.jpg`;

export const dataGetters = {
  artist: {
    image: genericImageGetter,
    title: nameGetter,
    subtitle: emptyReturnGetter
  },
  album: {
    image: genericImageGetter,
    title: nameGetter,
    subtitle: artistNameGetter
  },
  track: {
    image: trackImageGetter,
    title: nameGetter,
    subtitle: artistNameGetter
  }
};
