export const getDurationString = (track) => {
  const durationInSeconds = track.duration_ms / 1000;
  const seconds = Math.floor(durationInSeconds % 60);
  const secondsText = seconds < 10 ? `0${seconds}` : `${seconds}`;
  return `${Math.floor(durationInSeconds / 60)}:${secondsText}`;
};
