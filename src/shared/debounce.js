export function debounce(func, wait, immediate) {
  let timeout;
  return function() {
    const args = arguments;
    const callNow = immediate && !timeout;
    const context = this;
    const later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}
