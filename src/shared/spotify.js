import axios from 'axios';
import store from '../store';

export class Spotify {
  static #baseAuthUrl = 'https://accounts.spotify.com';
  static #baseApiUrl = 'https://api.spotify.com/v1';

  /**
   * AUTH REQUESTS
   */

  static async getToken() {
    const request = await axios(`${this.#baseAuthUrl}/api/token`, {
      data: 'grant_type=client_credentials',
      headers: {
        Authorization:
          'Basic YWU2YTNhMzFkZjVhNDI3YWI4Y2JjYWVjODZmNDJiMGI6ZTZjMTYzMWY0MzAyNDRmYWJhMGVhZGRiZmJmYjIwOTY=',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'POST'
    });

    localStorage.setItem('access_token', request.data.access_token);
    localStorage.setItem(
      'token_expiration',
      request.data.expires_in + Math.floor(Date.now() / 1000)
    );

    return request;
  }

  /**
   * SEARCH REQUESTS
   */
  static search(query = '', cancelTokenSource, itemTypes = ['artist', 'album', 'track']) {
    const concatItemTypes = itemTypes.reduce((accumulator, currentType, index) => {
      accumulator = index !== 0 ? `${accumulator},${currentType}` : `${currentType}`;
      return accumulator;
    }, '');
    const requestUrl = `${this.#baseApiUrl}/search?query=${query}&type=${concatItemTypes}`;
    return this.apiRequest(requestUrl, null, cancelTokenSource);
  }

  /**
   * ARTIST REQUESTS
   */

  static getArtistAlbums(artistId) {
    const url = `${this.#baseApiUrl}/artists/${artistId}/albums`;
    return this.apiRequest(url);
  }
  static getArtistRelatedArtists(artistId) {
    const url = `${this.#baseApiUrl}/artists/${artistId}/related-artists`;
    return this.apiRequest(url);
  }
  static getArtistTopTracks(artistId) {
    const url = `${this.#baseApiUrl}/artists/${artistId}/top-tracks`;
    return this.apiRequest(url, { market: 'ES' });
  }
  static getArtist(artistId) {
    const url = `${this.#baseApiUrl}/artists/${artistId}`;
    return this.apiRequest(url, { market: 'ES' });
  }

  /**
   * ALBUM REQUESTS
   */

  static getAlbum(albumId) {
    const url = `${this.#baseApiUrl}/albums/${albumId}`;
    return this.apiRequest(url);
  }
  static getAlbumTracks(albumId) {
    const url = `${this.#baseApiUrl}/albums/${albumId}/tracks`;
    return this.apiRequest(url);
  }

  /**
   * TRACK REQUESTS
   */

  static getTrack(trackId) {
    const url = `${this.#baseApiUrl}/tracks/${trackId}`;
    return this.apiRequest(url);
  }
  static getTrackAudioAnalysis(trackId) {
    const url = `${this.#baseApiUrl}/audio-analysis/${trackId}`;
    return this.apiRequest(url);
  }
  static getTrackAudioFeatures(trackId) {
    const url = `${this.#baseApiUrl}/audio-features/${trackId}`;
    return this.apiRequest(url);
  }

  /**
   * HELPER METHODS
   */

  static async apiRequest(url, params = {}, cancelTokenSource) {
    if (this.tokenNeedsRefresh()) {
      await this.getToken(store.getters.spotifyData);
    }
    const accessToken = localStorage.getItem('access_token');
    return axios({
      method: 'GET',
      url: url,
      cancelToken: cancelTokenSource ? cancelTokenSource.token : null,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      params
    }).catch();
  }

  static tokenNeedsRefresh() {
    const tokenExpiration = localStorage.getItem('token_expiration');
    return Math.floor(Date.now() / 1000) + 5 > tokenExpiration;
  }
}
