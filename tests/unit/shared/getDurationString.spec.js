import { track } from '../mocks/track.mock';
import { getDurationString } from '@/shared/getDurationString';

it('should return a string with the mm:ss format', () => {
  expect(getDurationString(track)).toBe('4:08');
});
