import { randomStringGenerator } from '../../../src/shared/randomStringGenerator';

it('should generate different strings when called multiple times', () => {
  const firstString = randomStringGenerator(16);
  const secondString = randomStringGenerator(16);
  expect(firstString).not.toEqual(secondString);
});
