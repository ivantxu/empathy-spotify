import { dataGetters } from '@/shared/dataGetters';

import { artist } from '../mocks/artist.mock';
import { album } from '../mocks/album.mock';
import { track } from '../mocks/track.mock';

it('should return an artist image', () => {
  expect(dataGetters.artist.image(artist)).toBe(
    'https://i.scdn.co/image/2b9294dc25928c9773d8d0b9e5b28a0bdf8d265c'
  );
});
it('should return an artist title', () => {
  expect(dataGetters.artist.title(artist)).toBe('Architects');
});
it('should return an artist subtitle', () => {
  expect(dataGetters.artist.subtitle(artist)).toBe(null);
});
it('should return an album image', () => {
  expect(dataGetters.album.image(album)).toBe(
    'https://i.scdn.co/image/ab67616d0000b273693c0365855ea5529b7490b4'
  );
});
it('should return an album title', () => {
  expect(dataGetters.album.title(album)).toBe('Holy Hell');
});
it('should return an album subtitle', () => {
  expect(dataGetters.album.subtitle(album)).toBe('Architects');
});
it('should return an track image', () => {
  expect(dataGetters.track.image(track)).toBe(
    'https://i.scdn.co/image/ab67616d0000b2730f08d4e55ebd266b7c3e8d74'
  );
});
it('should return an track title', () => {
  expect(dataGetters.track.title(track)).toBe('Doomsday');
});
it('should return an track subtitle', () => {
  expect(dataGetters.track.subtitle(track)).toBe('Architects');
});
