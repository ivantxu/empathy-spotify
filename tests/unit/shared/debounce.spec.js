import { debounce } from '@/shared/debounce';

describe('Debounce function', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.useRealTimers();
  });

  it('should execute only once if called in rapid succession', () => {
    const spy = jest.fn();
    const debouncedFunction = debounce(spy, 1000);
    debouncedFunction();
    debouncedFunction();
    jest.advanceTimersByTime(1001);
    expect(spy).toHaveBeenCalledTimes(1);
    debouncedFunction();
    jest.advanceTimersByTime(1200);
    expect(spy).toHaveBeenCalledTimes(2);
  });
});
