import { mount } from '@vue/test-utils';

import GenericThumbnail from '@/components/GenericThumbnail.vue';
import { album } from '../mocks/album.mock';
import { artist } from '../mocks/artist.mock';

it('Should display the right text', () => {
  const wrapper = mount(GenericThumbnail, {
    propsData: {
      data: album
    },
    stubs: ['RouterLink']
  });
  expect(wrapper.text()).toContain('Holy Hell');
});

it('Should display the right image and alt attribute', () => {
  const wrapper = mount(GenericThumbnail, {
    propsData: {
      data: album
    },
    stubs: ['RouterLink']
  });
  const thumbnail = wrapper.find('#thumbnail');
  expect(thumbnail.attributes().alt).toBe('Holy Hell');
  expect(thumbnail.attributes().style).toBe(
    'background-image: url(https://i.scdn.co/image/ab67616d0000b273693c0365855ea5529b7490b4);'
  );
});

it('should add a class to make artist thumbnail rounded', () => {
  const wrapper = mount(GenericThumbnail, {
    propsData: {
      data: artist
    },
    stubs: ['RouterLink']
  });
  const thumbnail = wrapper.find('#thumbnail');
  expect(thumbnail.attributes().class).toBe('round');
});
it('should not add a class to make the thumbnail rounded when it is not an artist', () => {
  const wrapper = mount(GenericThumbnail, {
    propsData: {
      data: album
    },
    stubs: ['RouterLink']
  });
  const thumbnail = wrapper.find('#thumbnail');
  expect(thumbnail.attributes().class).not.toBe('round');
});

it('Should navigate to detail on clicking', () => {
  const wrapper = mount(GenericThumbnail, {
    propsData: {
      data: album
    },
    stubs: ['RouterLink']
  });

  const toParam = { path: album.type, query: { id: album.id } };
  expect(wrapper.vm.toParam).toEqual(toParam);
});
