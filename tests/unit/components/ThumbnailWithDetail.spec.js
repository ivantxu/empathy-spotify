import { mount } from '@vue/test-utils';

import ThumbnailWithDetail from '@/components/ThumbnailWithDetail.vue';
import { album } from '../mocks/album.mock';

it('Should display the right title', () => {
  const wrapper = mount(ThumbnailWithDetail, {
    propsData: {
      data: album
    },
    stubs: ['RouterLink']
  });
  const thumbnail = wrapper.find('#title');
  expect(thumbnail.text()).toContain('Holy Hell');
});

it('Should display the right subtitle', () => {
  const wrapper = mount(ThumbnailWithDetail, {
    propsData: {
      data: album
    },
    stubs: ['RouterLink']
  });
  const thumbnail = wrapper.find('#subtitle');
  expect(thumbnail.text()).toContain('Architects');
});

it('Should display the right image and alt attribute', () => {
  const wrapper = mount(ThumbnailWithDetail, {
    propsData: {
      data: album
    },
    stubs: ['RouterLink']
  });
  const thumbnail = wrapper.find('#thumbnail');
  expect(thumbnail.attributes().alt).toBe('Holy Hell');
  expect(thumbnail.attributes().style).toBe(
    'background-image: url(https://i.scdn.co/image/ab67616d0000b273693c0365855ea5529b7490b4);'
  );
});

it('Should navigate to detail on clicking', () => {
  const wrapper = mount(ThumbnailWithDetail, {
    propsData: {
      data: album
    },
    stubs: ['RouterLink']
  });

  const toParam = { path: album.type, query: { id: album.id } };
  expect(wrapper.vm.toParam).toEqual(toParam);
});
