import { shallowMount } from '@vue/test-utils';
import BlurredHeader from '@/components/BlurredHeader.vue';

it('Should display the right title', () => {
  const wrapper = shallowMount(BlurredHeader, {
    propsData: {
      title: 'test'
    },
    stubs: ['router-link']
  });
  expect(wrapper.text()).toContain('test');
});

it('Should display the right subtitle', () => {
  const wrapper = shallowMount(BlurredHeader, {
    propsData: {
      subtitle: 'test'
    },
    stubs: ['router-link']
  });
  expect(wrapper.text()).toContain('test');
});

it('Should display the right image', () => {
  const wrapper = shallowMount(BlurredHeader, {
    propsData: {
      image: 'test'
    },
    stubs: ['router-link']
  });
  expect(wrapper.find('.blurred-background').attributes().style).toBe(
    'background-image: url(test);'
  );
});

it('Should navigate to search when clicking on the button', () => {
  const wrapper = shallowMount(BlurredHeader, {
    stubs: ['router-link']
  });
  expect(wrapper.find('#navigate-search').attributes().to).toBe('/search');
});
