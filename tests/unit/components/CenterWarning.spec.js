import { mount } from '@vue/test-utils';
import CenterWarning from '@/components/CenterWarning.vue';

it('Should display the right text in the label', () => {
  const wrapper = mount(CenterWarning, {
    propsData: {
      message: 'test'
    }
  });
  expect(wrapper.text()).toContain('test');
});
