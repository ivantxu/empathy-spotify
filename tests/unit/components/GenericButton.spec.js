import { mount } from '@vue/test-utils';
import GenericButton from '@/components/GenericButton.vue';

it('Should display the right text in the label', () => {
  const wrapper = mount(GenericButton, {
    propsData: {
      label: 'test'
    }
  });
  expect(wrapper.text()).toContain('test');
});

it('Should emit an event when clicked', async () => {
  const wrapper = mount(GenericButton, {
    propsData: {
      label: 'test'
    }
  });
  wrapper.find('button').trigger('click');
  expect(wrapper.emitted().clicked).toBeTruthy();
});
