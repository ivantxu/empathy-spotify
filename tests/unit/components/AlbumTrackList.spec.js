import { mount } from '@vue/test-utils';

import AlbumTrackList from '@/components/AlbumTrackList.vue';
import { album } from '../mocks/album.mock';
import { getDurationString } from '../../../src/shared/getDurationString';

it('Should have the right number of tracks', () => {
  const wrapper = mount(AlbumTrackList, {
    propsData: {
      album: album
    },
    stubs: ['RouterLink']
  });
  expect(wrapper.findAll('.track-container').length).toBe(11);
});

it('Should display the right track data', () => {
  const wrapper = mount(AlbumTrackList, {
    propsData: {
      album: album
    },
    stubs: ['RouterLink']
  });
  album.tracks.items.forEach((track, index) => {
    const listItem = wrapper.find(`.track-${index}`);
    expect(listItem.find('.track-index').text()).toBe(`${index + 1}`);
    expect(listItem.find('.thumbnail').attributes().style).toBe(
      `background-image: url(${album.images[0].url});`
    );
    expect(listItem.find('.title').text()).toBe(`${track.name}`);
    expect(listItem.find('.preview').exists()).toBe(track.preview_url !== '');

    expect(listItem.find('.duration').text()).toBe(getDurationString(track));
  });
});
