export const tracks = {
  href: 'https://api.spotify.com/v1/albums/5IwQtgxTKvh0TR2ZQ8RP8c/tracks?offset=0&limit=20',
  items: [
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 225040,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/53H9GpoVyBQWlaSCdOCOcl'
      },
      href: 'https://api.spotify.com/v1/tracks/53H9GpoVyBQWlaSCdOCOcl',
      id: '53H9GpoVyBQWlaSCdOCOcl',
      is_local: false,
      name: 'Death is Not Defeat',
      preview_url:
        'https://p.scdn.co/mp3-preview/78ef1146f173a8027a64bf7eb231e2b3fc5402a5?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 1,
      type: 'track',
      uri: 'spotify:track:53H9GpoVyBQWlaSCdOCOcl'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 255586,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/4sLhiw3JjfUmeEucLNrZ1v'
      },
      href: 'https://api.spotify.com/v1/tracks/4sLhiw3JjfUmeEucLNrZ1v',
      id: '4sLhiw3JjfUmeEucLNrZ1v',
      is_local: false,
      name: 'Hereafter',
      preview_url:
        'https://p.scdn.co/mp3-preview/91533c46494cab58c53ff529632a8227b3abd07a?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 2,
      type: 'track',
      uri: 'spotify:track:4sLhiw3JjfUmeEucLNrZ1v'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 219773,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/0dVqiFg1cssTbVBxUfJQ1i'
      },
      href: 'https://api.spotify.com/v1/tracks/0dVqiFg1cssTbVBxUfJQ1i',
      id: '0dVqiFg1cssTbVBxUfJQ1i',
      is_local: false,
      name: 'Mortal After All',
      preview_url:
        'https://p.scdn.co/mp3-preview/8ade04155b4d394b30b8561fa6a36a73d371c51e?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 3,
      type: 'track',
      uri: 'spotify:track:0dVqiFg1cssTbVBxUfJQ1i'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 253973,
      explicit: true,
      external_urls: {
        spotify: 'https://open.spotify.com/track/6TTpMwa4ld1hKRNQRFC0uF'
      },
      href: 'https://api.spotify.com/v1/tracks/6TTpMwa4ld1hKRNQRFC0uF',
      id: '6TTpMwa4ld1hKRNQRFC0uF',
      is_local: false,
      name: 'Holy Hell',
      preview_url:
        'https://p.scdn.co/mp3-preview/f1880422327e1f44923cdb7f674e16293df3b35e?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 4,
      type: 'track',
      uri: 'spotify:track:6TTpMwa4ld1hKRNQRFC0uF'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 248666,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/1QgVVDoxsRQOl791rLWtwI'
      },
      href: 'https://api.spotify.com/v1/tracks/1QgVVDoxsRQOl791rLWtwI',
      id: '1QgVVDoxsRQOl791rLWtwI',
      is_local: false,
      name: 'Damnation',
      preview_url:
        'https://p.scdn.co/mp3-preview/ca998d17f358e5a9cdcca633093387e4fdf12d71?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 5,
      type: 'track',
      uri: 'spotify:track:1QgVVDoxsRQOl791rLWtwI'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 241586,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/5moqI00BlhFQBtxR1Wo7dW'
      },
      href: 'https://api.spotify.com/v1/tracks/5moqI00BlhFQBtxR1Wo7dW',
      id: '5moqI00BlhFQBtxR1Wo7dW',
      is_local: false,
      name: 'Royal Beggars',
      preview_url:
        'https://p.scdn.co/mp3-preview/c89a32503a7b535f56d7b42ebbeea8869b3e832a?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 6,
      type: 'track',
      uri: 'spotify:track:5moqI00BlhFQBtxR1Wo7dW'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 253440,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/3QwNv5y4zmesKDsJxOz0GO'
      },
      href: 'https://api.spotify.com/v1/tracks/3QwNv5y4zmesKDsJxOz0GO',
      id: '3QwNv5y4zmesKDsJxOz0GO',
      is_local: false,
      name: 'Modern Misery',
      preview_url:
        'https://p.scdn.co/mp3-preview/0da50397f5c783cfe3bcd25a34546d920b9ee3b0?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 7,
      type: 'track',
      uri: 'spotify:track:3QwNv5y4zmesKDsJxOz0GO'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 230840,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/5eQansNXhKg5LpyYgcWUDD'
      },
      href: 'https://api.spotify.com/v1/tracks/5eQansNXhKg5LpyYgcWUDD',
      id: '5eQansNXhKg5LpyYgcWUDD',
      is_local: false,
      name: 'Dying to Heal',
      preview_url:
        'https://p.scdn.co/mp3-preview/516fb4063deb54401c874e819ff8e6db26a158ce?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 8,
      type: 'track',
      uri: 'spotify:track:5eQansNXhKg5LpyYgcWUDD'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 108666,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/0RhOCY7ltTu7wHy9avt0e1'
      },
      href: 'https://api.spotify.com/v1/tracks/0RhOCY7ltTu7wHy9avt0e1',
      id: '0RhOCY7ltTu7wHy9avt0e1',
      is_local: false,
      name: 'The Seventh Circle',
      preview_url:
        'https://p.scdn.co/mp3-preview/2cc087cd6b5a02e700864cdb04be819254574bea?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 9,
      type: 'track',
      uri: 'spotify:track:0RhOCY7ltTu7wHy9avt0e1'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 248880,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/4gadVHbMktnNYaKFUbWD3N'
      },
      href: 'https://api.spotify.com/v1/tracks/4gadVHbMktnNYaKFUbWD3N',
      id: '4gadVHbMktnNYaKFUbWD3N',
      is_local: false,
      name: 'Doomsday',
      preview_url:
        'https://p.scdn.co/mp3-preview/2d651a2fd57736691997e8e8e35ba49ba0765775?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 10,
      type: 'track',
      uri: 'spotify:track:4gadVHbMktnNYaKFUbWD3N'
    },
    {
      artists: [
        {
          external_urls: {
            spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv'
          },
          href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
          id: '3ZztVuWxHzNpl0THurTFCv',
          name: 'Architects',
          type: 'artist',
          uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
        }
      ],
      available_markets: ['AU', 'NZ'],
      disc_number: 1,
      duration_ms: 274933,
      explicit: false,
      external_urls: {
        spotify: 'https://open.spotify.com/track/6DfHBR4ruCPiVJKysSU63u'
      },
      href: 'https://api.spotify.com/v1/tracks/6DfHBR4ruCPiVJKysSU63u',
      id: '6DfHBR4ruCPiVJKysSU63u',
      is_local: false,
      name: 'A Wasted Hymn',
      preview_url:
        'https://p.scdn.co/mp3-preview/80ee6a7cec4bfa28179d114b1d5f12cfe6e3ef41?cid=ae6a3a31df5a427ab8cbcaec86f42b0b',
      track_number: 11,
      type: 'track',
      uri: 'spotify:track:6DfHBR4ruCPiVJKysSU63u'
    }
  ],
  limit: 20,
  next: null,
  offset: 0,
  previous: null,
  total: 11
};

export const tracksInData = { data: tracks };
