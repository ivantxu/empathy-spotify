export const artist = {
  external_urls: { spotify: 'https://open.spotify.com/artist/3ZztVuWxHzNpl0THurTFCv' },
  followers: { href: null, total: 470159 },
  genres: ['mathcore', 'melodic metalcore', 'metalcore', 'progressive metalcore', 'uk metalcore'],
  href: 'https://api.spotify.com/v1/artists/3ZztVuWxHzNpl0THurTFCv',
  id: '3ZztVuWxHzNpl0THurTFCv',
  images: [
    {
      height: 640,
      url: 'https://i.scdn.co/image/2b9294dc25928c9773d8d0b9e5b28a0bdf8d265c',
      width: 640
    },
    {
      height: 320,
      url: 'https://i.scdn.co/image/fcba450f66a488184485009c9984e98c4e010ce4',
      width: 320
    },
    {
      height: 160,
      url: 'https://i.scdn.co/image/14e3fdb6dac5ea3cfc3ee7b977c772f120455214',
      width: 160
    }
  ],
  name: 'Architects',
  popularity: 63,
  type: 'artist',
  uri: 'spotify:artist:3ZztVuWxHzNpl0THurTFCv'
};
