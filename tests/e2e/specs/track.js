const track = 'The 2nd Law';
const subtitle = 'The 2nd Law - Muse 4:59';

describe('track view', () => {
  before(() => {
    cy.request({
      url: 'https://accounts.spotify.com/api/token',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization:
          'Basic YWU2YTNhMzFkZjVhNDI3YWI4Y2JjYWVjODZmNDJiMGI6ZTZjMTYzMWY0MzAyNDRmYWJhMGVhZGRiZmJmYjIwOTY='
      },
      form: true,
      body: {
        grant_type: 'client_credentials'
      },
      json: true
    }).then((tokenRequest) => {
      localStorage.setItem('access_token', tokenRequest.body['access_token']);
      localStorage.setItem(
        'token_expiration',
        tokenRequest.body.expires_in + Math.floor(Date.now() / 1000)
      );
      const query = 'The 2nd Law';
      cy.visit(`/search?q=${query}`);
    });
  });
  beforeEach(() => {
    cy.restoreLocalStorageCache();
  });
  afterEach(() => {
    cy.saveLocalStorageCache();
  });
  it('should navigate to track detail view from search view', () => {
    cy.get(`[data-name="The 2nd Law: Isolated System"][data-type="track"]`).click();
  });

  it('should be in the right track view', () => {
    cy.get('h1').contains(track);
    cy.get('h2').contains(subtitle);
  });

  it('should go back to search', () => {
    cy.get('button').click();
    cy.url().should('contain', '/search');
  });
});
