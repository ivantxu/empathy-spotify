const artist = 'Muse';

describe('Artist view', () => {
  before(() => {
    cy.request({
      url: 'https://accounts.spotify.com/api/token',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization:
          'Basic YWU2YTNhMzFkZjVhNDI3YWI4Y2JjYWVjODZmNDJiMGI6ZTZjMTYzMWY0MzAyNDRmYWJhMGVhZGRiZmJmYjIwOTY='
      },
      form: true,
      body: {
        grant_type: 'client_credentials'
      },
      json: true
    }).then((tokenRequest) => {
      localStorage.setItem('access_token', tokenRequest.body['access_token']);
      localStorage.setItem(
        'token_expiration',
        tokenRequest.body.expires_in + Math.floor(Date.now() / 1000)
      );
      const query = artist;
      cy.visit(`/search?q=${query}`);
    });
  });
  beforeEach(() => {
    cy.restoreLocalStorageCache();
  });
  afterEach(() => {
    cy.saveLocalStorageCache();
  });
  it('should navigate to artist detail view from search view', () => {
    cy.get(`[data-name=${artist}][data-type="artist"] > p`).click();
  });

  it('should be in the right artist view', () => {
    cy.get('h1').contains('Muse');
  });

  it('should have top tracks', () => {
    cy.get('#top-tracks')
      .children()
      .should('have.length', 10);
  });
  it('should have albums', () => {
    cy.get('#albums')
      .children()
      .should('have.length', 20);
  });
  it('should have related artists', () => {
    cy.get('#related-artists')
      .children()
      .should('have.length', 20);
  });
  it('should go back to search', () => {
    cy.get('button').click();
    cy.url().should('contain', '/search');
  });
});
