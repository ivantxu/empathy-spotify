const album = 'The 2nd Law';

describe('Album view', () => {
  before(() => {
    cy.request({
      url: 'https://accounts.spotify.com/api/token',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization:
          'Basic YWU2YTNhMzFkZjVhNDI3YWI4Y2JjYWVjODZmNDJiMGI6ZTZjMTYzMWY0MzAyNDRmYWJhMGVhZGRiZmJmYjIwOTY='
      },
      form: true,
      body: {
        grant_type: 'client_credentials'
      },
      json: true
    }).then((tokenRequest) => {
      localStorage.setItem('access_token', tokenRequest.body['access_token']);
      localStorage.setItem(
        'token_expiration',
        tokenRequest.body.expires_in + Math.floor(Date.now() / 1000)
      );
      const query = album;
      cy.visit(`/search?q=${query}`);
    });
  });
  beforeEach(() => {
    cy.restoreLocalStorageCache();
  });
  afterEach(() => {
    cy.saveLocalStorageCache();
  });
  it('should navigate to album detail view from search view', () => {
    cy.get(`[data-name="${album}"][data-type="album"]`).click();
  });

  it('should be in the right album view', () => {
    cy.get('h1').contains(album);
  });

  it('should have tracks', () => {
    cy.get('#tracks li')
      .children()
      .should('have.length', 13);
  });
  it('should go back to search', () => {
    cy.get('button').click();
    cy.url().should('contain', '/search');
  });
});
