describe('Search', () => {
  before(async () => {
    const tokenRequest = await cy.request({
      url: 'https://accounts.spotify.com/api/token',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization:
          'Basic YWU2YTNhMzFkZjVhNDI3YWI4Y2JjYWVjODZmNDJiMGI6ZTZjMTYzMWY0MzAyNDRmYWJhMGVhZGRiZmJmYjIwOTY='
      },
      form: true,
      body: {
        grant_type: 'client_credentials'
      },
      json: true
    });
    localStorage.setItem('access_token', tokenRequest.body['access_token']);
    localStorage.setItem(
      'token_expiration',
      tokenRequest.body.expires_in + Math.floor(Date.now() / 1000)
    );
  });
  beforeEach(() => {
    cy.restoreLocalStorageCache();
  });
  afterEach(() => {
    cy.saveLocalStorageCache();
  });

  it('should search when the user inputs a query', () => {
    const query = 'Architects';
    cy.visit(`/search`);
    cy.get('input').type(query);
    cy.get(`[data-name="${query}"]`).contains(query);
  });
  it('should load previous search if there is a query parameter', () => {
    const query = 'Muse';
    cy.visit(`/search?q=${query}`);
    // cy.get(`[data-name="${query}"]`).contains(query);
    cy.get('[data-name="Muse"][data-type="artist"] > p').contains(query);
  });
  it('should clear the results when the input is cleared', () => {
    cy.get('input').clear();
    cy.wait(500);
    cy.get('h1').contains('No results');
  });
});
